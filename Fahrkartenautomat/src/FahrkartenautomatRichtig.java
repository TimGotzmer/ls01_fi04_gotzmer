
import java.util.Scanner;

class FahrkartenautomatRichtig {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double r�ckgabebetrag;

		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			r�ckgabebetrag = fahrkartenbestellung(zuZahlenderBetrag);
			fahrkartenAusgeben();
			r�ckgabeBetrag(r�ckgabebetrag);
			System.out.println("Neuer Kaufvorgang");
		} while (true);

	}

	public static void r�ckgabeBetrag(double r�ckgabebetrag) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		if (r�ckgabebetrag > 0.0) {
			System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe/fahrkartenAusgeben
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static double fahrkartenbestellung(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
			double AusstehenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f EURO\n", AusstehenderBetrag);
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return r�ckgabebetrag;
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderGesamtBetrag = 0;
		double zuZahlenderBetrag = 0;
		int anfrageTickets = 0;
		int ihreWahlTickets = 0;
		boolean falscheAuswahl = true;
		boolean nichtbezahlen = true;
		boolean anzahlStimmtNicht = true;
		
		String[] fahrkartenBezeichnung = new String[10];
		double[] fahrkartenPreis = new double[10];
	
		
		fahrkartenBezeichnung [0] = "Einzelfahrschein Berlin AB"; 
		fahrkartenBezeichnung [1] = "Einzelfahrschein Berlin BC"; 
		fahrkartenBezeichnung [2] = "Einzelfahrschein Berlin ABC";
		fahrkartenBezeichnung [3] = "Kurzstrecke"; 
		fahrkartenBezeichnung [4] = "Tageskarte Berlin AB"; 
		fahrkartenBezeichnung [5] = "Tageskarte Berlin BC"; 
		fahrkartenBezeichnung [6] = "Tageskarte Berlin ABC"; 
		fahrkartenBezeichnung [7] = "Kleingruppen-Tageskarte Berlin AB"; 
		fahrkartenBezeichnung [8] = "Kleingruppen-Tageskarte Berlin BC"; 
		fahrkartenBezeichnung [9] = "Kleingruppen-Tageskarte Berlin ABC"; 

		fahrkartenPreis [0] = 2.90; 
		fahrkartenPreis [1] = 3.30; 
		fahrkartenPreis [2] = 3.60; 
		fahrkartenPreis [3] = 1.90; 
		fahrkartenPreis [4] = 8.60; 
		fahrkartenPreis [5] = 9.00;
		fahrkartenPreis [6] = 9.60;
		fahrkartenPreis [7] = 23.50; 
		fahrkartenPreis [8] = 24.30; 
		fahrkartenPreis [9] = 24.90;
		
		while (nichtbezahlen) {
			falscheAuswahl = true;
			System.out.println("W�hlen Sie aus:");
			
			for(int i = 0; i < fahrkartenBezeichnung.length; i++) {
				System.out.println( i+1 + ". " + fahrkartenBezeichnung[i] + " " + fahrkartenPreis[i] + "0�");
				
				
			}
			System.out.println("11. W�hlen Sie ein Ticket");

			while (falscheAuswahl) {

				ihreWahlTickets = tastatur.nextInt();
			
				if (ihreWahlTickets <= 10 && ihreWahlTickets >= 1) {
		zuZahlenderBetrag = fahrkartenPreis[ihreWahlTickets-1];
					falscheAuswahl = false;
				} else if(ihreWahlTickets == 11 ){
					anzahlStimmtNicht = false;
					falscheAuswahl = false;
					nichtbezahlen = false;
				}
				else {
					anzahlStimmtNicht = true;
					System.out.print("Falsche eingabe, bitte w�hlen Sie ein Tiket oder bezahln!");
				}
			}

			

			while (anzahlStimmtNicht) {

				System.out.print("Anzahl Tickets eingeben 1 bis 10 Tikets:");
				anfrageTickets = tastatur.nextInt();

				if (anfrageTickets >= 11) {

					nichtbezahlen = false;
					System.out.println("Anzahl Tickets ist gr��er 10!");

				} else if (anfrageTickets <= 0) {
					anzahlStimmtNicht = true;
					System.out.println("Anzahl Tickets ist kleiner 1!");

				} else {
					
					System.out.println("Anzahl Tickets passt!");
					break;
				}
			}

			zuZahlenderGesamtBetrag = zuZahlenderGesamtBetrag + (zuZahlenderBetrag * anfrageTickets);
			zuZahlenderBetrag = 0;
			anfrageTickets = 0;
			System.out.printf("Momentan zu Zahlender Preis: %.2f �\n",  zuZahlenderGesamtBetrag );

		}
		return zuZahlenderGesamtBetrag;
	}
	
}
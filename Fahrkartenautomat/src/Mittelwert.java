
import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte x und y festlegen:
		// ===========================
		double[] x;
		int anzahl;
		double m;

		Scanner scan = new Scanner(System.in);

		System.out.print("Bitte geben Sie die Anzahl der Zahlen ein: ");
		anzahl = scan.nextInt();
		x = new double[anzahl];

		for (int i = 0; i < anzahl; i++) {
			System.out.print("Bitte geben Sie eine Zahl ein: ");
			x[i] = scan.nextDouble();
		}

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		m = berechneMittelwert(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		printArray(x);
		System.out.printf("Der Mittelwert ist %.2f\n", m);
	}

	public static double berechneMittelwert(double[] x) {
		double akku = 0;
		for (int i = 0; i < x.length; i++) {
			akku += x[i];
		}
		return akku / x.length;

	}

	public static void printArray(double[] x) {
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + ",");
		}
		System.out.println();
	}

}
